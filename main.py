#!/usr/bin/env python3
import os
import shlex
import sys
import time
from shutil import copy2
from subprocess import call
from subprocess import check_output
from typing import List

def set_blacklist(arg: list):
    global blacklist
    blacklist = arg

def package_libs(target: str, libs=[]):
    otool_command = shlex.split("otool -L {}".format(target))
    output = check_output(otool_command).decode()

    for path in get_lib_paths_to_copy(output):
        if not path in libs:
            libs.append(path)
            package_libs(path, libs)

    return libs

def get_lib_paths_to_copy(otool_output: str) -> List[str]:
    result = []
    for line in otool_output.splitlines():
        if line.endswith(":"):
            continue

        line = line.strip()
        if not line.startswith("/usr/local"):
            continue
        if line.startswith(tuple(blacklist)):
            continue
        path_name = line.split(" ")[0]
        result.append(path_name)
    return result


def copy_file(target_directory: str, file_to_copy: str) -> str:
    file_name = os.path.basename(file_to_copy)
    if not os.path.isdir(target_directory): os.makedirs(target_directory)
    resulting_path = os.path.join(target_directory, file_name)

    if not os.path.exists(resulting_path):
        print("copying: {} => {}".format(file_to_copy, resulting_path), end='\r')
        copy2(file_to_copy, resulting_path)
        adjust_permissions(resulting_path)

    return resulting_path


def adjust_permissions(file: str):
    os.chmod(file, 0o777)

def rename_dynamic_path(original_path: str):
    lib_name = os.path.basename(original_path)
    print("renaming: {} => @executable_path/../Frameworks/{}".format(original_path, lib_name), end='\r')
    adjust_permissions("{bundle}.app/Contents/MacOS/{bundle}".format(bundle=bundle))
    adjust_permissions("{bundle}.app/Contents/Frameworks/{lib}".format(lib=lib_name, bundle=bundle));

    call(shlex.split(
        "install_name_tool -change {original_path} "
        "@executable_path/../Frameworks/{lib_name} {bundle}.app/Contents/MacOS/{bundle}".format(
            original_path=original_path, lib_name=lib_name, bundle=bundle
            )
        ))

    call(shlex.split(
        "install_name_tool -id "
        "@executable_path/../Frameworks/{lib_name} {bundle}.app/Contents/Frameworks/{lib_name}".format(
            original_path=original_path, lib_name=lib_name, bundle=bundle
            )
        ))


if __name__ == '__main__':
    start_time = time.time();
    args = sys.argv[1:]
    if not "Darwin" in os.uname():
        exit(0)
    if len(args) == 0 :
        print("Usage: [bundle name (NO .app AT THE END!)] (library folder blacklist...)")
        exit(1)

    bundle = args[0]
    set_blacklist(args[1:])
    if '/' in bundle:
        print("Bundle must be in current directory!")
        exit(2)

    libs = package_libs("{bundle}.app/Contents/MacOS/{bundle}".format(bundle=bundle))
    for file in libs:
        copy_file("{bundle}.app/Contents/Frameworks".format(bundle=bundle), file)

    for currentLib in libs:
        olib = os.path.basename(currentLib)
        rename_dynamic_path(currentLib)
        for filename in libs:
            adjust_permissions("{bundle}.app/Contents/Frameworks/{lib_name}".format(bundle=bundle, lib_name=os.path.basename(filename)))
            call(shlex.split(
                "install_name_tool -change {original_path} "
                "@executable_path/../Frameworks/{olib} {bundle}.app/Contents/Frameworks/{lib_name}".format(
                    original_path=currentLib, olib=olib, lib_name=os.path.basename(filename), bundle=bundle
                    )
                ))
    
    print("\n\nOperation over. Processed {} libraries in {} seconds".format(str(len(libs)), (time.time() - start_time)))
