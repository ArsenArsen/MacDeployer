## Mac deploying thingy
`Usage: [bundle name (NO .app AT THE END!)] (library folder blacklist...)`

It'll copy all libraries from /usr/local whose paths don't start with anything from the blacklist  
Needs XCode selected.  
Hugely helped by [I Al Istannen](https://github.com/I-Al-Istannen/)

## License
Check `UNLICENSE`
